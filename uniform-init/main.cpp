#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

struct Aggregate
{
    int a;
    double b;
};

long long int get_number()
{
    static int value;
    return  ++value;
}

struct Data
{
    long long int a;
    double b;

    Data(double b) : a{get_number()}, b{b}
    {}

    Data(int a, double b)
        : a{a}, b{b} // uniform init syntax
    {}
};

TEST_CASE("C++98 - init was a mess")
{
    int a(5); // direct initialization
    int b(); // resetting to zero - may cause a problem

    int c = 5;
    int d = int(6);

    Aggregate agg1 = { 1, 3.14 };
    Aggregate agg2 = {};

    Data d1(1, 3.14);  // constructor called

    int tab[] = { 1, 2, 3, 4, 5 };

    vector<int> vec;
    vec.push_back(1);
    vec.push_back(2);
    vec.push_back(3);

    map<int, string> dict;
    dict.insert(make_pair(1, "one"));
    dict.insert(make_pair(2, "two"));
    dict.insert(make_pair(3, "three"));
}

Data get_data()
{
    return {1, 3.14};
}

TEST_CASE("C++11 - uniform initialization syntax")
{
    int a{5}; // direct initialization

    int b{};
    REQUIRE(b == 0);

    int c = 5;
    int d = int{5};

    Aggregate agg1 = { 1, 3.14 };
    Aggregate agg2 = {};

    Data d1{1, 3.14}; // constructor call

    int tab[] = { 1, 2, 3, 4, 5 };

    const vector<int> vec = { 1, 2, 3, 4, 5};
    const vector<int> data{1, 2, 3, 4, 5};
    const map<int, string> dict ={ { 1, "one"}, { 2, "two"}, {3, "three"} };
}

TEST_CASE("init with bracket is safer")
{
    int x{ get_number() }; // ERROR in clang

    Data d1{3.14};

    REQUIRE(d1.a == 2);
}


TEST_CASE("initializer list")
{
    vector<int> vec1{1, 2, 3, 4, 5}; // vector(initializer_list<int>)
    vector<int> vec2 = {1, 2, 3, 4, 5}; // vector(initializer_list<int>)

    vec1.insert(vec1.begin(), { 1, 2, 3, 4, 5 });
}

class Container
{
    vector<int> vec_;

public:
    using iterator = vector<int>::iterator; // typedef vector<int>::iterator iterator;
    using const_iterator = vector<int>::const_iterator;

    Container(std::initializer_list<int> il) : vec_(il)
    {
    }

    void add(int item)
    {
        vec_.push_back(item);
    }

    iterator begin()
    {
        return vec_.begin();
    }

    iterator end()
    {
        return vec_.end();
    }
};

TEST_CASE("iterating over custom container")
{
    Container cont = { 1, 2, 3 };

    for(const auto& item : cont)
    {
        cout << item << " ";
    }
    cout << endl;
}

TEST_CASE("initializer_list & oveloaded contructors")
{
    SECTION("() - calls vector<int>(size_t, int)")
    {
        vector<int> vec(10, 1);

        REQUIRE(vec.size() == 10);
        REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x == 1;}));
    }

    SECTION("{} - calls vector<int>(initializer_list<int>)")
    {
        vector<int> vec{10, 1};

        REQUIRE(vec.size() == 2);
        REQUIRE((vec[0] == 10 && vec[1] == 1));
    }
}

TEST_CASE("auto with = {}")
{
    auto il1 = { 1, 2, 3, 4 }; // initalizer_list<int>

    auto il2 = { 3.14 }; // initalizer_list<double>
}

template <typename Key>
using Dictionary = map<Key, string>;

TEST_CASE("using as template alias")
{
    Dictionary<int> dict1 = { { 1, "one"}, {2, "two"} };
    Dictionary<string> pl_en = { { "jeden", "one"}, { "dwa", "two"} };

}
