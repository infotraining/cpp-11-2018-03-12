#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>
#include <stack>

using namespace std;

namespace Ver_1
{
    class Gadget
    {
        int id_;

    public:
        Gadget()          = default;
        virtual ~Gadget() = default;

        Gadget(int id)
            : id_{id}
        {
        }

        int id() const
        {
            return id_;
        }
    };
}

namespace Ver_2
{
    int get_id()
    {
        static int id;
        return ++id;
    }

    class Gadget
    {
        int id_ = get_id();
        string name_{"not-set"};
        double price_ = 0.99;

    public:
        Gadget()          = default;
        virtual ~Gadget()
        {
            cout << "~Gadget(id: " << id_ << ")\n";
        }

        Gadget(int id)
            : Gadget{id, "unknown", 0.0} // delegating constructor
        {
            cout << "Constructing Gadget(id:  " <<  id_ << ")\n";
            throw std::runtime_error("Error#2");
        }

        Gadget(int id, const string& name, double price) : id_{id}, name_{name}, price_{price}
        {
            //throw std::runtime_error("Error#1");
        }

        int id() const
        {
            return id_;
        }

        string name() const
        {
            return name_;
        }

        double price() const
        {
            return price_;
        }

        virtual void play() const
        {
            cout << "Gadget(" << name_ << ") is playing..." << endl;
        }
    };

    class SuperGadget : public Gadget
    {
    public:
        using Gadget::Gadget; // inheritance of constructors

        void play() const override // compiler checks if method from base class is overriden
        {
            cout << "SuperGadget(" << name() << ") is playing louder..." << endl;
        }
    };
}

TEST_CASE("constructors")
{
    using namespace Ver_2;

    Gadget g; // constructed using default constructor
    REQUIRE(g.id() == 1);
    REQUIRE(g.name() == "not-set");
    REQUIRE(g.price() == Approx(0.99));

    try
    {
        Gadget g1{10};
        REQUIRE(g1.id() == 10);
        REQUIRE(g1.name() == "unknown");
    }
    catch(const runtime_error& e)
    {
        cout << "Caught: " << e.what() << endl;
    }

    Gadget g2;
    REQUIRE(g2.id() == 2);

    SuperGadget sg{1, "boom-box", 999.99};
    Gadget& ref_g = sg;

    ref_g.play();
}



class Nocopyable
{
public:
    Nocopyable(const Nocopyable&) = delete;
    Nocopyable& operator=(const Nocopyable&) = delete;
    Nocopyable()                             = default;
};

int calculate(int value)
{
    cout << "calculate(" << value << ")\n";

    return value * value;
}

int calculate(double) = delete;

namespace Generic
{
    inline namespace Cpp11
    {
        template <typename T, typename = typename enable_if<is_integral<T>::value>::type>
        T calculate(T value)
        {
            return value * value;
        }
    }

    namespace Cpp14
    {
        template <typename T, typename = enable_if_t<is_integral<T>::value>>
        T calculate(T value)
        {
            return value * value;
        }
    }

//    namespace Future
//    {
//        template <Integral>
//        Integral calculate(Integral value)
//        {
//            return value * value;
//        }
//    }
}

TEST_CASE("noncopyable class")
{
    Nocopyable nc;

    //Nocopyable copy_nc = nc;  // ERROR - copy constructor is deleted

    // calculate(3.14); // ERROR - calculate(double) is deleted
    // calculate(3.14f); // ERROR

    auto result1 = Generic::calculate(5);
    REQUIRE(result1 == 25);

    auto result2 = Generic::Cpp14::calculate(10);
    REQUIRE(result2 == 100);
}


class Command;
class UndoableCommand;

class CommandHistory
{
    stack<UndoableCommand*> history_;
public:
    void save(UndoableCommand* cmd);
    UndoableCommand* pop_last();
};

class Command
{
public:
    virtual ~Command() = default;
    virtual void execute() = 0;
    virtual void undo() = 0;
};

class UndoableCommand : public Command
{
    CommandHistory history_;
public:
    void execute() override final
    {
        do_save_state();
        history_.save(clone());
        do_action();
    }

    void undo() override final
    {
        auto cmd = history_.pop_last();
        cmd->do_undo();
    }

    virtual UndoableCommand* clone() const = 0;
protected:
    virtual void do_save_state() = 0;
    virtual void do_action() = 0;
    virtual void do_undo();
};

class PasteCmd : public UndoableCommand
{
    // UndoableCommand interface
protected:
    void do_save_state();
    void do_action();
    void do_undo();
};

