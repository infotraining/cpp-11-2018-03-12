#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

void foo(int* ptr)
{
    if (ptr)
        cout << "foo(int*: " << *ptr << ")" << endl;
    else
        cout << "foo(int*: " << 0 << ")" << endl;
}

void foo(long value)
{
    cout << "foo(long: " << value << ")\n";
}

void foo(nullptr_t)
{
    cout << "foo(nullptr_t)" << endl;
}

TEST_CASE("nullptr is better NULL")
{
   int* ptr = nullptr;

   REQUIRE(ptr == 0);
   REQUIRE(ptr == nullptr);
}

TEST_CASE("why nullptr is better than NULL")
{
    int x = 10;

    foo(&x);
    foo(NULL); // call: foo(long) instead of foo(int*)
    foo(nullptr);

    int* ptr = nullptr;
    foo(ptr);
}
