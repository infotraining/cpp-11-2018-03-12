#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <list>
#include <memory>
#include <vector>

using namespace std;

string full_name(const string& first_name, const string& last_name)
{
    return first_name + " " + last_name;
}

TEST_CASE("reference binding rules")
{
    int x = 10;

    SECTION("C++98")
    {
        SECTION("l-value can be bound to l-value ref")
        {
            int& ref_x = x;
        }

        SECTION("r-value can be bound to l-value ref to const")
        {
            const string& ref_full_name = full_name("Jan", string("Kowalski"));
        }
    }

    SECTION("C++11")
    {
        SECTION("r-value can be bound to r-value ref")
        {
            string&& rv_ref_full_name = full_name("Jan", "Kowalski");

            rv_ref_full_name.clear();
        }

        SECTION("l-value cannot be bound to r-value ref")
        {
            string text = "txt";

            //string&& rv_ref = text;
        }
    }
}

class Bitmap
{
    char* image_;
    size_t size_;
    string path_;

public:
    Bitmap()
        : Bitmap{"deafult"}
    {
    }

    Bitmap(const string& path)
        : image_{new char[path.length()]}
        , size_{path.length()}
        , path_{path}
    {
        copy(path.begin(), path.end(), image_);

        cout << "Bitmap(" << path_ << ")" << endl;
    }

    ~Bitmap()
    {
        delete[] image_;
        cout << "~Bitmap(" << path_ << ")";

        if (image_ == nullptr)
            cout << " - state after move";
        cout << endl;
    }

    Bitmap(const Bitmap& source)
        : image_{new char[source.size_]}
        , size_{source.size_}
        , path_{source.path_}
    {
        copy(source.image_, source.image_ + size_, image_);

        cout << "Bitmap(cc: " << path_ << ")" << endl;
    }

    Bitmap& operator=(const Bitmap& source)
    {
        if (this != &source)
        {
            delete[] image_;

            image_ = new char[source.size_];
            size_  = source.size_;
            path_  = source.path_;

            copy(source.image_, source.image_ + size_, image_);

            cout << "Bitmap::operator=(cc: " << path_ << ")" << endl;
        }

        return *this;
    }

    // move constructor
    Bitmap(Bitmap&& source) noexcept
        : image_{source.image_}
        , size_{source.size_}
        , path_{std::move(source.path_)}
    {
        source.image_ = nullptr; // setting to resourceless state for source object
        source.size_  = 0; // optionally

        cout << "Bitmap(mv: " << path_ << ")" << endl;
    }

    // move assignment operator
    Bitmap& operator=(Bitmap&& source) noexcept(is_nothrow_move_assignable<string>::value)
    {
        if (this != &source)
        {
            delete[] image_;

            image_ = source.image_;
            size_  = source.size_;
            path_  = std::move(source.path_);

            source.image_ = nullptr; // setting to resourceless state for source object

            cout << "Bitmap::operator=(mv: " << path_ << ")" << endl;
        }

        return *this;
    }

    void draw() const
    {
        cout << "Drawing: ";

        for (size_t i = 0; i < size_; ++i)
            cout << image_[i];

        cout << endl;
    }
};

TEST_CASE("using bitmap")
{
    Bitmap bmp1("bmp1");
    bmp1.draw();

    Bitmap bmp2 = bmp1;
    bmp2.draw();

    Bitmap bmp3("temp");
    bmp3 = bmp1;

    Bitmap bmp4 = move(bmp3); // move constructor
    bmp4.draw();

    bmp1 = move(bmp4); // move assignment
}

Bitmap load_bitmap_from_file(const string& path)
{
    Bitmap bmp{path};

    return bmp;
}

TEST_CASE("more complex")
{
    cout << "\n------------------\n";

    Bitmap background{"background"};

    vector<Bitmap> bitmaps;
    bitmaps.reserve(10);

    bitmaps.push_back(std::move(background));
    bitmaps.push_back(Bitmap("drawing1"));
    bitmaps.push_back(Bitmap("drawing2"));
    bitmaps.push_back(Bitmap("drawing3"));
    bitmaps.push_back(load_bitmap_from_file("drawing4"));

    cout << "\n------------------\n";
}

class Sprite
{
    Bitmap bmp_;

public:
    Sprite(const string& path)
        : bmp_{path}
    {
        cout << "Sprite(" << this << ")" << endl;
    }

    Sprite(const Sprite&) = default;
    Sprite& operator=(const Sprite&) = default;

    Sprite(Sprite&& source) = default;
    Sprite& operator=(Sprite&&) = default;

    ~Sprite()
    {
        cout << "~Sprite(" << this << ")" << endl;
    }

    void render(int x, int y) const
    {
        bmp_.draw();
        cout << "sprite at pos: (" << x << ", " << y << ")\n";
    }
};

TEST_CASE("game")
{
    cout << "\n--------------------------\n";

    vector<Sprite> sprites = {Sprite("s1"), Sprite("s2"), Sprite("s3")};
    sprites.reserve(100);

    for (int i = 1; i < 10; ++i)
        sprites.push_back(Sprite("sprite" + to_string(i)));

    cout << "\n";

    for (const auto& s : sprites)
        s.render(19, 44);

    cout << "%%%%%%%%%%%%%%%%%%%%%%%%" << endl;

    vector<Sprite> new_sprites = move(sprites);

    REQUIRE(sprites.size() == 0);

    cout << "%%%%%%%%%%%%%%%%%%%%%%%%" << endl;

    list<Sprite> lst_sprites(make_move_iterator(new_sprites.begin()), make_move_iterator(new_sprites.end()));

    for (const auto& s : lst_sprites)
        s.render(87, 523);
}

TEST_CASE("push_back to vector")
{
    cout << "\n@@@@@@@@@@@@@@@@@@@@@@@@@@\n";

    Bitmap background{"background"};

    vector<Bitmap> bitmaps;
    //bitmaps.reserve(100);

    for (int i = 1; i <= 16; ++i)
        bitmaps.push_back(Bitmap("test" + to_string(i)));

    bitmaps.push_back(Bitmap("last"));

    cout << "\n------------------\n";
}

// move-only type
template <typename T>
class UniquePtr
{
    T* ptr_;

public:
    explicit UniquePtr(T* ptr)
        : ptr_{ptr}
    {
    }

    UniquePtr(const UniquePtr&) = delete;
    UniquePtr& operator=(const UniquePtr&) = delete;

    UniquePtr(UniquePtr&& source) noexcept
        : ptr_{source.ptr_}
    {
        source.ptr_ = nullptr;
    }

    UniquePtr& operator=(UniquePtr&& source) noexcept
    {
        if (this != source)
        {
            delete ptr_;
            ptr_        = source.ptr_;
            source.ptr_ = nullptr;
        }

        return *this;
    }

    ~UniquePtr()
    {
        delete ptr_;
    }

    T* get() const
    {
        return ptr_;
    }

    T& operator*() const
    {
        return *ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }

    explicit operator bool()
    {
        return ptr_ != nullptr;
    }
};

namespace Explanation
{
    unique_ptr<Bitmap> create_bitmap(const string& path) // ver1
    {
        return unique_ptr<Bitmap>(new Bitmap(path));
    }

    unique_ptr<Bitmap> create_bitmap(string& path) // ver2
    {
        return unique_ptr<Bitmap>(new Bitmap(path));
    }

    unique_ptr<Bitmap> create_bitmap(string&& path) // ver3
    {
        return unique_ptr<Bitmap>(new Bitmap(move(path)));
    }
}

// perfect forwarding
template <typename T>
unique_ptr<Bitmap> create_bitmap(T&& path)
{
    return unique_ptr<Bitmap>(new Bitmap(forward<T>(path)));
}

template <typename T, typename Arg>
unique_ptr<T> create_unique(Arg&& arg)
{
    return unique_ptr<T>(new T(forward<Arg>(arg)));
}

TEST_CASE("creating bitmap with forwarding")
{
    string path1       = "path1";
    const string path2 = "path2";

    auto p1 = create_bitmap(path1); // 1
    auto p2 = create_bitmap(path2); // 2
    auto p3 = create_bitmap(string("path3")); // 3
}

void use(Bitmap* bmp)
{
    if (bmp)
        bmp->draw();
}

void use(unique_ptr<Bitmap> bmp)
{
    if (bmp)
        bmp->draw();
}

void safe_code()
{
    create_bitmap("bmp1"); // ok

    auto bmp2 = create_bitmap("bmp2");
    use(bmp2.get()); // ok

    unique_ptr<Bitmap> bmp = create_bitmap("bmp3");

    bmp = make_unique<Bitmap>("bmp3"); // C++14

    use(bmp.get()); // ok
    use(move(bmp));

    use(create_bitmap("bmp4")); // ok

    vector<unique_ptr<Bitmap>> bitmaps;

    bitmaps.push_back(create_bitmap("vecbmp1"));
    bitmaps.push_back(create_bitmap("vecbmp2"));
    bitmaps.push_back(move(bmp2));

    for (const auto& bptr : bitmaps)
    {
        bptr->draw();
    }
}

namespace LegacyCode
{
    Bitmap* create_bitmap(const string& path)
    {
        return new Bitmap(path);
    }

    Bitmap* create_bitmaps(size_t size)
    {
        return new Bitmap[size];
    }

    void use(Bitmap* bmp)
    {
        if (bmp)
            bmp->draw();
    }

    void use_and_destroy(Bitmap* bmp)
    {
        if (bmp)
            bmp->draw();
        delete bmp;
    }

    void leaky_code()
    {
        create_bitmap("bmp1"); // leak

        LegacyCode::use(create_bitmap("bmp2")); // leak

        Bitmap* bmp = create_bitmap("bmp3");

        LegacyCode::use(bmp); // ok
        use_and_destroy(bmp);

        bmp->draw(); // segfault
    }
}

TEST_CASE("unique_ptr move semantics")
{
    cout << "\n&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n";
    ;

    unique_ptr<Bitmap> uptr1 = create_bitmap("dynamic_bmp");

    uptr1->draw();
    (*uptr1).draw();

    unique_ptr<Bitmap> uptr2 = std::move(uptr1);

    REQUIRE(uptr1.get() == nullptr);
    uptr2->draw();

    safe_code();
}

TEST_CASE("unique_ptr with dynamic arrays")
{
    size_t size   = 10;

    SECTION("C++98")
    {
        Bitmap* table = LegacyCode::create_bitmaps(size);

        for (size_t i = 0; i < size; ++i)
            table[i].draw();

        delete[] table;
    }

    SECTION("C++11")
    {
        unique_ptr<Bitmap[]> table(LegacyCode::create_bitmaps(size));

        for (size_t i = 0; i < size; ++i)
            table[i].draw();
    } // automatic delete[]
}

string get_text_rvo()
{
    return string("text");
}

string get_text_nrvo()
{
    string str = "text";

    for (int i = 1; i < 10; ++i)
        str += to_string(i);

    return str;
}

TEST_CASE("return value optimization")
{
    string text = get_text_nrvo(); // may be optimized avoiding copies
}

TEST_CASE("shared_ptr")
{
    cout << "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";

    map<string, shared_ptr<Bitmap>> bitmaps;

    {
        shared_ptr<Bitmap> bmp = make_shared<Bitmap>("shared_bmp"); // rc: 1

        {
            shared_ptr<Bitmap> bmp2 = bmp; // rc: 2

            bmp->draw();
            bmp2->draw();

            bitmaps.insert(make_pair("background", bmp)); // // rc: 3

            cout << "Use count: " << bmp.use_count() << endl;
        } // rc: 2

        cout << "Use count: " << bmp.use_count() << endl;

    } // rc: 1

    cout << "Use count: " << bitmaps["background"].use_count() << endl;

    bitmaps["background"]->draw();

    bitmaps.clear(); // rc: 0

    cout << "END of scope" << endl;
}
