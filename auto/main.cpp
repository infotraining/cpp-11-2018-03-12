#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

template <typename T>
void deduce_type_of_arg(T arg)
{
}

TEST_CASE("C++98 - automatic type deduction only for templates")
{
    deduce_type_of_arg(42); // deduce_type_of_arg(int arg)
    deduce_type_of_arg(3.14); // deduce_type_of_arg(double arg)
}

TEST_CASE("auto")
{
    int x        = 10;
    const int cx = 20;
    double pi    = 3.14;
    string str   = "text";

    SECTION("can be used to deduce type of variable")
    {
        SECTION("using literals")
        {
            //auto ax0; // ERRROR - auto must be initialized
            auto ax1 = 10; // int
            auto ax2 = 10u; // unsigned int
            auto ax3 = 3.14; // double
            auto ax4 = 3.14f; // float
            auto ax5 = "text"; // const char*
        }

        SECTION("using variables")
        {
            auto ax1 = x; // int
            auto ax2 = cx; // int
            auto ax3 = pi; // double
            auto ax4 = str; // string

            auto ax5 = &x; // int*
            auto ax6 = &cx; // const int*
        }
    }

    SECTION("auto can be decrated with const, volatile, refs & pointers")
    {
        auto* ptr1       = &x;
        const auto* ptr2 = &cx; // const int*

        auto& ref1       = x; // int&
        const auto& ref2 = x; // const int&
    }
}

void foo(int)
{
}

TEST_CASE("auto rules")
{
    int x             = 10;
    const int cx      = 10;
    int& ref_x        = x;
    const int& cref_x = x;

    SECTION("case 1 - auto without ref")
    {
        SECTION("reference (with const & volatile) is stripped away")
        {
            auto ax1 = ref_x; // int
            auto ax2 = cref_x; // int
        }

        SECTION("const & volatile are stripped away")
        {
            auto ax3 = cx; // int
        }

        SECTION("static table decays to pointer")
        {
            int tab[10];

            auto atab = tab; // int*
        }

        SECTION("function decays to pointer")
        {
            auto afun = foo; // void(*afun)(int)
        }
    }

    SECTION("case 2 - auto with ref or *")
    {
        SECTION("reference (with const & volatile) is preserved")
        {
            auto& ax1 = ref_x; // int&
            auto& ax2 = cref_x; // const int&

            auto& ax3 = cx; // const int&
        }

        SECTION("static table does not decay to pointer")
        {
            int tab[10];

            auto& reftab = tab; // int(&)[10]
        }

        SECTION("function does not decay to pointer")
        {
            auto& reffun = foo; // void(&afun)(int)
        }
    }
}

int get_number()
{
    return 42;
}

TEST_CASE("use case for auto")
{
    SECTION("iterating over std container")
    {
        vector<int> vec = {1, 2, 3, 4, 5};

        SECTION("C++98")
        {
            for (vector<int>::const_iterator it = vec.begin(); it != vec.end(); ++it)
            {
                cout << *it << " ";
            }
            cout << endl;
        }

        SECTION("C++11")
        {
            for (auto it = vec.cbegin(); it != vec.cend(); ++it)
            {
                cout << *it << " ";
            }
            cout << endl;
        }
    }

    SECTION("type of variable is unknown")
    {
        auto l = [] { cout << "Lambda" << endl; };
    }

    SECTION("sometimes safer code")
    {
        SECTION("C++98")
        {
            auto s = get_number();
        }
    }
}

TEST_CASE("auto syntax")
{
    SECTION("direct")
    {
        auto x(5); // int x(5);

        SECTION("C++11")
        {
            int x1{5}; // int x = 5;
            auto x2{5}; // initializer_list<int> x{5}
        }

        SECTION("C++17")
        {
            int x1{5}; // int x = 5;
            auto x2{5}; // int x{5};
        }
    }

    SECTION("copy")
    {
        auto x = 5;
    }
}

TEST_CASE("decltype - alternate type deduction mechanism")
{
    vector<int> vec = {1, 2, 3, 4, 5};

    decltype(vec) vec2; // vector<int>
    REQUIRE(vec2.size() == 0);

    auto vec3 = vec; // vector<int>
    REQUIRE_THAT(vec3, Catch::Matchers::Equals(vector<int>{1, 2, 3, 4, 5}));

    map<int, string> m = {{1, "one"}};

    //m[2]; // modifies m

    decltype(m[2]) item = m[1]; // string&
}

struct Vector2D
{
    double x, y;
};

double operator*(Vector2D a, Vector2D b)
{
    return a.x * b.x + a.y * b.y;
}

template <typename T>
auto multiply(const T& a, const T& b) -> decltype(a * b)
{
    return a * b;
}

TEST_CASE("multiply")
{
    Vector2D vec1{1.0, 5.6};
    Vector2D vec2{0.0, 1.0};

    auto result = multiply(vec1, vec2);
    static_assert(is_same<double, decltype(result)>::value, "Error");

    REQUIRE(result == Approx(5.6));
}

namespace Cpp98
{
    template <typename Iterator>
    typename iterator_traits<Iterator>::value_type sum(Iterator first, Iterator last)
    {
        typedef typename iterator_traits<Iterator>::value_type ResultType;

        ResultType result = ResultType();

        for (Iterator it = first; it != last; ++it)
            result += *it;

        return result;
    }
}

namespace Cpp11
{
    template <typename Iterator>
    auto sum(Iterator first, Iterator last) -> typename decay<decltype(*first)>::type
    {
        using ResultType = typename decay<decltype(*first)>::type;

        ResultType result{};

        for (auto it = first; it != last; ++it)
            result += *it;

        return result;
    }
}

namespace Cpp14
{
    template <typename Iterator>
    auto sum(Iterator first, Iterator last)
    {
        using ResultType =  decay_t<decltype(*first)>;

        ResultType result{};

        for (auto it = first; it != last; ++it)
            result += *it;

        return result;
    }
}

TEST_CASE("sum of vector")
{
    const vector<int> vec = {1, 2, 3};

    int result = Cpp11::sum(vec.begin(), vec.end());

    REQUIRE(result == 6);
}
