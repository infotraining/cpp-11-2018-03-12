#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

TEST_CASE("range based for")
{
   SECTION("works with container")
   {
       vector<int> vec = { 1, 2, 3, 4, 5, 6 };

       for(int item : vec)
       {
           cout << item << " ";
       }
       cout << endl;

       SECTION("is interpreted as")
       {
           for(auto it = vec.begin(); it != vec.end(); ++it)
           {
               int item = *it;
               cout << item << " ";
           }
       }
   }

   SECTION("works with static arrays")
   {
       int tab[10] = { 1, 2, 3, 4, 5, 6, 7 };

       for(const auto& item : tab)
       {
           cout << item << " ";
       }
       cout << endl;

       SECTION("is interpreted as")
       {
           for(auto it = std::begin(tab); it != std::end(tab); ++it)
           {
               const auto& item = *it;
               cout << item << " ";
           }
       }
   }

   SECTION("works with initializer_list")
   {
       for(int item : { 1, 2, 3, 4, 5, 6 })
       {
           cout << item << " ";
       }
       cout << endl;
   }
}

TEST_CASE("range-based-for catch")
{
    vector<string> words = { "one", "two", "three" };

    SECTION("inefficient impl")
    {
        for(string word : words)
            cout << word << " ";
        cout << endl;
    }

    SECTION("efficient impl")
    {
        for(const string& word : words)
            cout << word << " ";
        cout << endl;

        SECTION("with auto")
        {
            for(const auto& word : words)
                cout << word << " ";
            cout << endl;
        }
    }
}


template <typename T>
class Container
{
    vector<T> vec_;

public:
    typedef typename vector<T>::iterator iterator;

    void add(const T& item)
    {
        vec_.push_back(item);
    }

    void add(T&& item)
    {
        vec_.push_back(std::move(item));
    }

    iterator begin()
    {
        return vec_.begin();
    }

    iterator end()
    {
        return vec_.end();
    }
};

TEST_CASE("iterating over custom container")
{
    Container<string> cont;

    cont.add("1");
    cont.add("2");
    cont.add("3");

    for(const auto& item : cont)
    {
        cout << item << " ";
    }
    cout << endl;

    SECTION("works in the same way as for_each")
    {
        for_each(cont.begin(), cont.end(), [](string item) { cout << item << " "; });
        cout << endl;
    }
}