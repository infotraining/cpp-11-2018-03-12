#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

enum Coffee : uint8_t; // forward declaration

void drink(Coffee); // ok with forward declaration

enum Coffee : uint8_t { espresso, doppio, lura = 128 }; // definition of enum

void drink(Coffee coffee) // impl
{
    switch(coffee)
    {
    case espresso:
        cout << "Drinking espresso" << endl;
        break;
    case doppio:
        cout << "Drinking doppio" << endl;
        break;
    case lura:
        cout << "WTF!" << endl;
        break;
    };
}

TEST_CASE("enums")
{
   Coffee coffee = espresso;

   drink(coffee);

   uint8_t value = coffee;

   coffee = static_cast<Coffee>(128);

   drink(coffee);
}

enum class Engine : uint8_t { diesel = 1, tdi = 2, petrol = 4, hybrid = 8 };

TEST_CASE("scoped enum")
{
    Engine engine = Engine::petrol;

    int value = static_cast<int>(engine);

    engine = static_cast<Engine>(8);

    REQUIRE(engine == Engine::hybrid);

    cout << "Engine type: " << static_cast<int>(engine) << endl;
}

TEST_CASE("using underlying_type trait")
{
    typedef underlying_type<Engine>::type EngineIntegralType;

    static_assert(is_same<EngineIntegralType, uint8_t>::value, "Error");
}
