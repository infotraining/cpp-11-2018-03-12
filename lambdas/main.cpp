#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <algorithm>
#include <cassert>
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <numeric>
#include <vector>

using namespace std;

// closure class
class Lambda_23472547364
{
public:
    void operator()() const
    {
        cout << "Lambda!!!" << endl;
    }
};

namespace Explain
{
    template <typename Iterator, typename Function>
    void for_each(Iterator first, Iterator end, Function f)
    {
        for (auto it = first; it != end; ++it)
            f(*it);
    }
}

void print(int x)
{
    cout << "print item: " << x << endl;
}

TEST_CASE("lambda")
{
    auto l = []() { cout << "Lambda!!!" << endl; };
    l();
    l();

    SECTION("is iterpreted as")
    {
        auto l = Lambda_23472547364{};

        l();
        l();
    }

    SECTION("can deduce return type")
    {
        auto multiply = [](int a, int b) { return a * b; };

        REQUIRE(multiply(4, 5) == 20);
        REQUIRE(multiply(2, 8) == 16);
    }

    SECTION("in c++11 for complex lambda use ->")
    {
        auto describe = [](int x) -> string {
            if (x % 2 == 0)
                return "even";
            else
                return string("odd");
        };

        REQUIRE(describe(2) == "even");
        REQUIRE(describe(1) == "odd");
    }

    SECTION("use as function parameters")
    {
        vector<int> vec = {1, 2, 3, 4};

        Explain::for_each(vec.begin(), vec.end(), [](int x) { cout << "item: " << x << endl; });
        Explain::for_each(vec.begin(), vec.end(), print);

        SECTION("auto allows avoiding duplicates")
        {
            vector<int> data(20);
            iota(data.begin(), data.end(), 1);

            auto print_item = [](int x) { cout << "item: " << x << endl; };

            Explain::for_each(data.begin(), data.end(), print_item);
            Explain::for_each(vec.begin(), vec.end(), print_item);
        }
    }
}

// closure class
class Lambda_2344234234
{
    const int threshold_;

public:
    Lambda_2344234234(int t)
        : threshold_{t}
    {
    }

    bool operator()(int x) const
    {
        return x > threshold_;
    }
};

TEST_CASE("capturing local variables")
{
    vector<int> vec = {5, 234, 634, 23, 1334, 534, 2452, 55, 6235};

    int threshold = 500;

    SECTION("capture by value")
    {
        auto predicate = [threshold](int x) { return x > threshold; };

        threshold = 1000; // does not change value of captured variable

        auto pos = find_if(vec.begin(), vec.end(), predicate);

        REQUIRE(*pos == 634);
    }

    SECTION("capture by reference")
    {
        long sum{};

        for_each(vec.begin(), vec.end(), [&sum](int x) { return sum += x; });

        cout << "Sum: " << sum << endl;
    }

    SECTION("both ways")
    {
        int counter{};

        for_each(vec.begin(), vec.end(), [threshold, &counter](int x) { if (x > threshold) ++counter; });

        REQUIRE(counter == count_if(vec.begin(), vec.end(), [threshold](int x) { return x > threshold; }));
    }
}

int threshold = 1000;

TEST_CASE("trap")
{
    auto predicate = [=](int x) { return x > threshold; };

    threshold = 4000;

    //REQUIRE(predicate(2000) == true); // test failed
}

struct Printer
{
    void operator()(int x)
    {
        cout << "Printer item: " << x << endl;
    }
};

TEST_CASE("storing lambda")
{
    SECTION("auto")
    {
        auto l = [] { cout << "lambda" << endl; };
        l();
    }

    SECTION("non-capture lambda can be stored in function ptr")
    {
        void (*ptr_fun)(int) = [](int x) { cout << "x: " << x << endl; };

        ptr_fun(10);
        ptr_fun(20);
    }

    SECTION("std::function")
    {
        function<void(int)> f;

        f = &print;
        f(10);

        f = Printer();
        f(20);

        string msg = "Print from lambda: ";
        f          = [msg](int x) { cout << msg << x << endl; };
    }
}

namespace Cpp14
{
    auto create_generator(int seed)
    {
        return [seed]() mutable { return ++seed; };
    }
}

std::function<int()> create_generator(int seed)
{
    return [seed]() mutable { return ++seed; };
}

TEST_CASE("generators")
{
    auto gen = create_generator(100);

    REQUIRE(gen() == 101);
    REQUIRE(gen() == 102);
    REQUIRE(gen() == 103);
}

// closure class
class Lambda_23499547364
{
public:
    template<typename T>
    void operator()(T x) { cout << x << endl; }
};

TEST_CASE("new features in C++14")
{
    auto l = [](auto x) { cout << x << endl; };

    l(10);
    l(3.14);
    l("string"s);

    auto eats_all = [](auto... x) { };

    eats_all(1, 3.14, "string");
}
