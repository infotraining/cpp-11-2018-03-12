#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>
#include <tuple>
#include <numeric>

using namespace std;

tuple<int, int, double> calc_stats(const vector<int>& data)
{
    auto min_pos = min_element(data.begin(), data.end());
    auto max_pos = max_element(data.begin(), data.end());

    double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return make_tuple(*min_pos, *max_pos, avg);
}

TEST_CASE("tuples")
{
    tuple<int, double, string> t1{1, 3.14, "text"};

    SECTION("getting to elems")
    {
        REQUIRE(get<0>(t1) == 1);

        get<1>(t1) += 3.14;
        cout << get<1>(t1) << endl;

        // get<5>(t1); // compiler error
    }

    SECTION("function can return many values")
    {
        vector<int> vec = { 1, 2, 3, 4 };

        auto stats = calc_stats(vec);

        cout << "Min: " << get<0>(stats) << endl;
        cout << "Max: " << get<1>(stats) << endl;
        cout << "Avg: " << get<2>(stats) << endl;

        SECTION("better using tie()")
        {
            int min, max;
            //double avg;

            tie(min, max, ignore) = calc_stats(vec);

            cout << "Min: " << min << endl;
            cout << "Max: " << max << endl;
            // cout << "Avg: " << avg << endl;
        }

//        SECTION("C++17")
//        {
//            auto[min, max, avg] = calc_stats(vec);
//        }
    }
}

struct Data
{
    int a;
    double b;
    string c;

    bool operator<(const Data& d) const
    {
        return tie(a, b, c) < tie(d.a, d.b, d.c);
    }
};


