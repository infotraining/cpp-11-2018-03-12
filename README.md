# README #

## Docs ##

* https://infotraining.bitbucket.io/cpp-11/

## Sharing folder in VirtualBox ##

* dodaj udostępniony katalog w ``Urządzenia/Udostępniane foldery``

* wprowadź w terminalu:

```
mkdir shared_host
sudo mount -t vboxsf shared ~/shared-host/
```

gdzie:

  * ``shared`` - nazwa udostępnionego katalogu w ustawieniach maszyny wirtualnej


## C++ Core Guidelines ##

* https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md