#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

TEST_CASE("raw-strings")
{
   SECTION("\n\t\b have special meaning for string")
   {
       string path = "c:\\nasz katalog\\twoj katalog\\backup";

       cout << path << endl;
   }

   SECTION("C++11 - we have raw strings")
   {
       string path = R"(c:\nasz katalog\twoj katalog\backup)";

       cout << path << endl;

       SECTION("multi-line string")
       {
           string multiline = R"(Line1
Line2
Line3)";

           cout << multiline << endl;
       }
   }

   SECTION("support for custom delimiters")
   {
       string raw = R"radmor(cytat:"(hello world)")radmor";

       cout << raw << endl;
   }
}
